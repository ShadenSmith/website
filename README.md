---
layout: page
title: HowTo
sidebar_link: true 
---

# Tensor World webpage
(powered by the [Hydeout](https://fongandrew.github.io/hydeout/) theme for Jekyll)

This website is meant as a portal for the tensor decomposition community to
share information and collaborate on the open project Tensor_World. This
website function is, on top of project related communication, to facilitate
open and reproducible research, as well as referencing. 

Indeed, this repository makes it easy and accessible for anyone to write a blog
post on the website, add content to an existing page (such as the list of
existing toolboxes), or to even add a page devoted to a specific topic.

### Joining the project

Before anything else, if you are interested in the project and want to
contribute, you will likely need to join the gitlab tensor_world group. for
this,
   - Create a Gitlab account (you can loggin with a Github account).
   - Send an email to tensor.world@gmail.com and ask for rights in the project.
     You will then be able to add and modify files in all projects inside the
     tensor_world group.

### Site organization

Pages are contained in the pages directory, and blog posts are contained in
the \_posts directory. You should not need to open other directories for adding
content (unless you want to modify the webpage appearnace, which I do no
recommand).

### Usage

As a researcher on tensor decomposition methods, you are likely to detain
helpful information to all the community on
- how to developp an open source
tensor toolbox for such a large community
- how we should procede in practice (organization of seminars - hackathons -
  special sessions - workshops, program structure etc)
- foundings and general organization

To share all these informations easily, you are very welcome to write a post on
the website. You can keep in track with what is posted online by subscribing to
the [RSS flux]({{site.baseurl}}/{{site.feed.path | default: 'feed.xml' }} ).

#### How to write a post, step by step

I will now explain how to post (it is very simple) directly on Gitlab, but you
can of course clone the repository on your local machine and push your post written offline.

###### Step 1: Create a markdown file

Open the \_posts folder. Using the + icon, create a new file. The name <b> HAS
TO START </b> with a date, preferably the date you are writing the post. For
instance, the 18st January 2054, you would write:

```
2054-01-18-anything-works.md
```

You may add any name after the date in the new file name, but do not forget the .md
extension.

###### Step 2: Adding a header to your .md file

Second, you need to write a very simple header, so that Jekyll (the web site
generator) can process it.

This goes as follows:
```
---
layout: post
title: your-post-title
category:
  - Discussions
author: your-name
excerpt_separator:  <!--more-->
---
```
Simply copy-paste these lines of code, replacing the title and the name. 

###### Step 3: Write your post in Markdown (basically plain text), and/or/also HTML

You are all set to starting writing. The .md format allows to type using
text-like syntax, more information is provided [here](https://daringfireball.net/projects/markdown/syntax). I suggest to use other
posts as models for the syntax, but basically just write it like you would in a
plain text editor.

If you already know how to write in HTML, you can also use it to add links or
small pictures.

Finally, if you post is long, use the <!--more--> tag to cut the post in two: a
part shown in the blog summary (preferably only a paragraph), and a second long
part shown when clicking on the post on the website.


### Heavy modding

Additionally, you may contribute to the structure of the website itself by
playing around with the features of the Hydeout theme for Jekyll. For
information on how to do that, please refer to the [Hydeout github
repository](https://github.com/fongandrew/hydeout).
