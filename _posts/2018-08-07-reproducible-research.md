---
layout: post
title: "My take on why we need Tensor_World"
category:
  - Discussions
author: Jeremy Cohen
excerpt_separator:  <!--more-->
---

Tensor_World is my way to start a movement for more open and reproducible
algorithmic research in the tensor decomposition community. As of now, every
research group has his own toolbox, and they are disparate in their contents
and use. Therefore, it can be cumbersome to compare one's new method with
existing ones because so many different software should be handled, with
various inputs, outputs and specifications.

<!--more-->

On this website, a [table](/pages/toolboxes/) of all the existing toolboxes in various languages
that can compute any tensor decomposition model is being built in order for
everyone to naviguate this software jungle. As one can see, we are going in a
direction where concertation is mandatory, otherwise we will keep implementing
the same functions over and over again, while older well-implemented softwares
are forgotten.

In the end, what I am aiming for is easy reproducibility. Things should be as
simple as this: write you code in
whatever language, convert it to a package for Tensor_World (and benefit from well-implemented routines - one may think about the CPD/PARAFAC), and compare with acknowledge methods all implemented by
their authors as packages to the core libraries of Tensor_World.

I may be aiming for too much, but this is what I think we should be going for.
Along the way, I am setting a few tools for enabling fluid and easy
collaborations among us (Gitlab group repository, Google groups mailing list,
this webpage, see links in the nav bar). Feel free to join me in this project
by adding content anywhere you see fit, posting on this website what you think
of this project, or just come talk to me in the next conference.


