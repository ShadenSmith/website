---
layout: page
sidebar_link: true
permalink: /toolboxes/
title: Toolbox and links
---

This table is a tentative to collect all existing tensor decomposition
toolboxes. Please contribute if you notice any toolbox missing or any outdated
data.

|Name |Institution |Language  |Key words  |Link| Maintained |
|-|-|
| 3WayPack | Leiden University | Fortran | psychometrics, interface | [3mode compagny](http://three-mode.leidenuniv.nl/) | yes |
|-|-|
| AOADMM | University of Minnesota | Matlab |constrained decomposition, proximal methods | [K. Huang's page](https://sites.google.com/a/umn.edu/huang663/)| ? |
|-|-|
| multiway package | University of Minnesota | R | psychometrics, indscale | [N. Helwig's page](http://users.stat.umn.edu/~helwig/software.html) | ? |
|-|-|
| N-way | University of Copenhaguen | Matlab | chemometrics, ALS, PARAFAC2 | [@Mathworks](https://uk.mathworks.com/matlabcentral/fileexchange/1088-the-n-way-toolbox) | yes |
|-|-|
| SPLATT | University of Minnesota | C |  sparsity, parallel computing, Big Data | [@Github](https://github.com/ShadenSmith/splatt/)| yes? | 
|-|-|
| Tensorbox | Riken | Matlab | neuroscience | [A. Phan's page](http://www.bsp.brain.riken.jp/~phan/#tensorbox)| ? |
|-|-|
| Tensorlab (v3)| KU Leuven| Matlab | structure, second order methods | [Tensorlab](https://www.tensorlab.net/)| yes |
|-|-|
| Tensor toolbox | Sandia National Laboratories | Matlab | storage, sparsity, first order methods | [@Sandia](https://www.sandia.gov/~tgkolda/TensorToolbox/index-2.6.html) | yes |
|-|-|
| TTpy | Skoltech | Python | tensor train | [@Github](https://github.com/oseledets/ttpy) | yes? |
|-|-|
| TT-toolbox | Skoltech | Matlab | tensor train | [@Github](https://github.com/oseledets/TT-Toolbox) | yes? |
|-|-|

Also, tensor codes can be found at the following pages, but do not constitute
proper toolboxes:
- [J. J. Li's page](http://fruitfly1026.github.io/), which has similar content
  to SPLATT.
- [Tensor package](http://www.gipsa-lab.grenoble-inp.fr/~pierre.comon/TensorPackage/tensorPackage.html)
- [J.E.Cohen's repository](https://github.com/cohenjer/Tensor_codes)
- [G. Zhou's page](http://bsp.brain.riken.jp/~zhougx/tensor.html)
- [P. Tichavsky's page](http://si.utia.cas.cz/downloadPT.htm)
- many more...


